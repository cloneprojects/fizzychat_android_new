package com.app.fizzychat.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.app.fizzychat.R;
import com.app.fizzychat.baseUtils.AsyncTaskCompleteListener;
import com.app.fizzychat.baseUtils.Const;
import com.app.fizzychat.baseUtils.PostHelper;
import com.app.fizzychat.baseUtils.SharedHelper;
import com.app.fizzychat.baseUtils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class OTPVerfication_activity extends AppCompatActivity implements AsyncTaskCompleteListener {

    private String number = "", code = "", isDriveLogin = "", driveLoginTime = "";

    private void Setheme(String themevalue) {
        switch (themevalue) {
            case "1":
                setTheme(R.style.AppThemeGreen);
                break;
            case "2":
                setTheme(R.style.AppThemeBlue);
                break;
            case "3":
                setTheme(R.style.AppThemeIndigo);
                break;
            case "4":
                setTheme(R.style.AppThemeGrey);
                break;
            case "5":
                setTheme(R.style.AppThemeYellow);
                break;
            case "6":
                setTheme(R.style.AppThemeOrange);
                break;
            case "7":
                setTheme(R.style.AppThemePurple);
                break;
            case "8":
                setTheme(R.style.AppThemePaleGreen);
                break;
            case "9":
                setTheme(R.style.AppThemelightBlue);
                break;
            case "10":
                setTheme(R.style.AppThemePink);
                break;
            case "11":
                setTheme(R.style.AppThemelightGreen);
                break;
            case "12":
                setTheme(R.style.AppThemelightRed);
                break;
            default:
                setTheme(R.style.AppThemeGreen);
                break;
        }
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String themevalue = SharedHelper.getKey(this, "theme_value");
        Setheme(themevalue);
        setContentView(R.layout.activity_otpverfication_activity);

        Utils.enableStrictMode();

        TextView verify_number = (TextView) findViewById(R.id.verify_number);
        TextView makeCall = (TextView) findViewById(R.id.makeCall);
        TextView verify_number_label = (TextView) findViewById(R.id.verify_number_lbl);
        final EditText otp_number = (EditText) findViewById(R.id.edt_otp_number);


        final Intent intent = getIntent();
        if (intent != null) {
            number = intent.getStringExtra("number");
            code = intent.getStringExtra("code");
            isDriveLogin = intent.getStringExtra("isDriveBackUp");
            driveLoginTime = intent.getStringExtra("driveLoginTime");
            verify_number.setText(getResources().getString(R.string.verify) + " +" + code + " " + number);
            String wrong = "<font color='#1abc9c'>" + getResources().getString(R.string.wrong) + " </font>";
            //noinspection deprecation
            verify_number_label.setText(Html.fromHtml(getText(R.string.verify_number) + "\n" + intent.getStringExtra("number") + " " + wrong));
            // otp_number.setText(intent.getStringExtra("OTP"));

        }

        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(OTPVerfication_activity.this, GettingPhoneNumber_activity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });

        findViewById(R.id.makeCall).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                makeCall(code, number);
            }
        });

        verify_number_label.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        findViewById(R.id.otp_next).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (otp_number.getText().toString().matches("") || otp_number.getText().toString().length() < 4) {
                    Utils.showShortToast(getResources().getString(R.string.valid_otp), getApplicationContext());
                } else {
                    Register(code, number, otp_number.getText().toString().trim());

//                    Intent intent = new Intent(OTPVerfication_activity.this, InitProfile_activity.class);
//                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                    intent.putExtra("number", number);
//                    intent.putExtra("code", code);
//                    startActivity(intent);
//                    finish();

                   /*
                    if (isDriveLogin.equalsIgnoreCase("true")) {

                        Intent intent = new Intent(OTPVerfication_activity.this, DownloadGoogleDriveFIle.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("isDriveLogin", "true");
                        intent.putExtra("driveLoginTime", driveLoginTime);
                        startActivity(intent);
                        finish();

                    } else {


                        Intent intent = new Intent(OTPVerfication_activity.this, InitProfile_activity
                                .class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("number", number);
                        intent.putExtra("code", code);
                        startActivity(intent);
                        finish();
                    }*/
                }

            }
        });
    }

    private void Register(String countryCode, String number, String otp) {


        if (!Utils.isNetworkAvailable(this)) {
            Utils.showShortToast(getResources().getString(R.string.no_internet), this);
            //progressBar.setVisibility(View.GONE);
            return;
        }
        Utils.showSimpleProgressDialog(OTPVerfication_activity.this, "Please wait...", false);
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Const.COUNTRY_CODE, countryCode);
            jsonObject.put(Const.PHONE, number);
            jsonObject.put(Const.OTP, otp);

            Log.e("otp_verify", jsonObject.toString());
            new PostHelper(Const.Methods.VERIFY, jsonObject.toString(), Const.ServiceCode.VERIFY, this, OTPVerfication_activity.this);
        } catch (JSONException e) {
            e.printStackTrace();
            Utils.removeProgressDialog();
        }


    }

    private void makeCall(String countryCode, String number) {


        if (!Utils.isNetworkAvailable(this)) {
            Utils.showShortToast(getResources().getString(R.string.no_internet), this);
            //progressBar.setVisibility(View.GONE);
            return;
        }
        Utils.showSimpleProgressDialog(OTPVerfication_activity.this, "Please wait...", false);
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Const.COUNTRY_CODE, countryCode);
            jsonObject.put(Const.PHONE, number);

            Log.e("otp_verify", jsonObject.toString());
            new PostHelper(Const.Methods.VERIFY_CALL, jsonObject.toString(), Const.ServiceCode.VERIFY_CALL, this, OTPVerfication_activity.this);
        } catch (JSONException e) {
            e.printStackTrace();
            Utils.removeProgressDialog();
        }


    }

    @Override
    public void onTaskCompleted(JSONObject response, int serviceCode) {
        Utils.removeProgressDialog();
        Log.e("otp_verify", "response:" + response + " service: " + serviceCode);
        switch (serviceCode) {
            case Const.ServiceCode.VERIFY:


                if (response.optString("error").equalsIgnoreCase("false")) {
                    Log.e("otp_verify", "response:" + response.optJSONObject("message").optString("success"));

                    if (response.optJSONObject("message").optString("success").equalsIgnoreCase("true")) {
                        Intent intent = new Intent(OTPVerfication_activity.this, InitProfile_activity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("number", number);
                        intent.putExtra("code", code);
                        startActivity(intent);
                        finish();

                    } else {
                        Utils.showShortToast(response.optJSONObject("message").optString("message"), getApplicationContext());
                    }
                } else {
                    Utils.showShortToast(getResources().getString(R.string.incorrect_otp), getApplicationContext());
                }
                break;

            case Const.ServiceCode.VERIFY_CALL:

                break;
        }
    }
}
